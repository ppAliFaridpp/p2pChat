import logging
import threading
import socket
import uuid

from network.flood.accept import (
    get_listen_broadcast_socket, get_new_connection, respond_to_broadcast
)
from network.flood.broadcast import (
    chat_request_network, receive_responses, get_broadcast_socket,
    serialize_packet, deserialize_broadcast_response_packet)
from network.connect.core import (
    get_empty_port, get_tcp_socket, start_listening,
    start_listening_thread)

BROADCAST_MESSAGE = 'Hello'
IP = '0.0.0.0'
_PACKET_SIZE = 4096


class ChatInterface(object):
    listen_connection_socket: socket
    available_connections_dict = dict()
    available_users = set()
    socket_dict = dict()

    def __init__(self):
        self.listen_connection_socket = get_listen_broadcast_socket()
        self.uid = uuid.uuid4().__str__()

    def start_accepting_connections_thread(self):
        t = threading.Thread(target=self._start_accepting_connections)
        t.start()

    def _start_accepting_connections(self):
        while True:
            data, addr = get_new_connection(self.listen_connection_socket)
            t = threading.Thread(target=self._handle_new_connection, args=(data, addr,))
            t.start()

    def _handle_new_connection(self, data, addr):
        logging.debug('new connection request from: {}'.format(addr))
        if not (data and addr):
            return
        port = get_empty_port()
        tcp_socket = get_tcp_socket(IP, port)
        respond_to_broadcast(self.listen_connection_socket, serialize_packet(uid=self.uid, port=port), addr)
        new_conn, new_addr = tcp_socket.accept()
        logging.debug('***************')
        data = new_conn.recv(_PACKET_SIZE)
        logging.debug('new_conn.accept : {}'.format(data))
        deser_data = deserialize_broadcast_response_packet(data)
        self.add_to_socket_dict(deser_data['uid'], new_conn)
        logging.debug('port {} will be bind for request from {}:'.format(port, addr))
        start_listening(new_conn)

    def get_available_users_from_network(self):
        s = get_broadcast_socket()
        chat_request_network(s, self.get_broadcast_packet())
        received_users = receive_responses(s)
        for user in received_users:
            new_socket = self.connect_to_socket(user)
            start_listening_thread(new_socket)
        logging.debug('received users: {}'.format(received_users))
        logging.debug('all available_users: {}'.format(self.available_users))

    def connect_to_socket(self, user):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            logging.debug('Connecting to : {}'.format(user))
            s.connect(user[1])
            try:
                self.send_message(s, "None")
                self.add_to_socket_dict(user[0], s)
                return s
            except BrokenPipeError:
                pass
        except OSError as e:
            logging.debug(e)
        return s

    def check_and_send_message(self, target_uid, msg):
        logging.info('trying to message {} to {}'.format(msg, target_uid))
        try:
            self.send_message(self.socket_dict[target_uid], msg)
            logging.info('sent message {} to {}'.format(msg, target_uid))
        except BrokenPipeError as e:
            logging.debug(e)
            try:
                self.socket_dict.pop(target_uid)
            except KeyError:
                pass
            try:
                self.socket_dict.pop(target_uid)
            except KeyError:
                pass
            return False
        return True

    def send_message(self, s, msg):
        pkt = serialize_packet(uid=self.uid, msg=msg)
        s.sendall(pkt)
        logging.debug('Sending message: {} to {}'.format(pkt, s))

    def get_broadcast_packet(self):
        return serialize_packet(uid=self.uid, msg=BROADCAST_MESSAGE)

    def add_to_socket_dict(self, uid, s):
        self.socket_dict[uid] = s
