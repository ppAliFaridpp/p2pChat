import json
import socket
import logging

_PACKET_SIZE = 4096

# TODO add to env

IP = '0.0.0.0'
LISTEN_PORT = 8000
INTRODUCTION_MESSAGE = 'Hello'


def handle_request():
    pass


def get_listen_broadcast_socket():
    server_address = (IP, LISTEN_PORT)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(server_address)
    logging.debug('broadcast listener bind to {}'.format(server_address))
    return s


def get_new_connection(s: socket.socket):
    en_data, addr = s.recvfrom(_PACKET_SIZE)
    dec_data = deserialize_packet(en_data)
    logging.debug('new message on broadcast server: {}'.format(dec_data))
    if dec_data['msg'] == INTRODUCTION_MESSAGE:
        return dec_data, addr


def respond_to_broadcast(s: socket.socket, data: str, addr: tuple):
    s.sendto(data, addr)


def deserialize_packet(pkt):
    return json.loads(pkt.decode('utf-8'))
